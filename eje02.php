<!DOCTYPE html>
<html>
<head>
	<title>Uso de metodos GET</title>
</head>
<body>
	<h2>Conversion Decimal a Binario</h2>
	<form name="frmPost" method="GET">


		Escriba el Decimal:
		<input type="text" name="txtNumero">
		<input type="submit" name="btnEnviar">


	</form>

	<?php

		if(!empty($_GET['txtNumero']))
		{
			$numeroDecimal = $_GET['txtNumero'];
			echo "El numero decimal es: ". $numeroDecimal."<br>";

			$numeroBinario = ' ';

			do {
				$numeroBinario  = ($numeroDecimal % 2) .  $numeroBinario;
				$numeroDecimal = (int)($numeroDecimal/2);
			} while ( $numeroDecimal > 0);

			echo "Numero Binario: " . $numeroBinario;

		}
	?>

</body>
</html>