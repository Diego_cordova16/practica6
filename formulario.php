<!DOCTYPE html>
<html>
<head>
	<title>Captura de datos</title>
</head>
<body>
	<h2>Captura de Datos</h2>
	<form name="formulario" method="POST" action="procesar.php"> <!-- action indica el tipo de accion que va a realiar en este caso que la informacion enviada sera manehada por el archivo procesar.php-->
		<table border="1"><!--la etiqueta table se utiliza para crear tablas-->
			<tr><!--Define una nueva fila-->
				<td>Nombres:</td><!--Define una celda-->
				<td><input type="text" name="txtNombre" size= 50></td><!--Size establece el tamaño del textbox-->
			</tr>
			<tr>
				<td>Apellidos:</td>
				<td><input type="text" name="txtApellido" size=50></td>
			</tr>
			<tr>
				<td>Edad:</td>
				<td><input type="text" name="txtEdad" size=50></td>

			</tr>
			<tr>
				<td colspan=2><input type="submit" name="btnEnviar"></td><!--Colspan se utiliza para combinar columnas-->

			</tr>
		</table>
	</form>
</body>
</html>